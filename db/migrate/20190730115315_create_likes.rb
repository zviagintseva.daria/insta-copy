class CreateLikes < ActiveRecord::Migration[5.2]
  def change
    create_table :likes do |t|

      t.timestamps
    end

	add_reference :likes, :user, index: true
	add_reference :likes, :photo, index: true
    add_index :likes, [:user_id, :photo_id], unique: true
  end
end
