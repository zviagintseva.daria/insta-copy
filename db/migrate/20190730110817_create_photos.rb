class CreatePhotos < ActiveRecord::Migration[5.2]
  def change
    create_table :photos do |t|

      t.timestamps
    end
    
		add_reference :photos, :user, index: true
  end
end
