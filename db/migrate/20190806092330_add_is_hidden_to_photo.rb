class AddIsHiddenToPhoto < ActiveRecord::Migration[5.2]
  def change
    add_column :photos, :is_hidden, :boolean, default: false
  end
end
