# frozen_string_literal: true

# # frozen_string_literal: true

# # This file should contain all the record creation needed to seed the database with its default values.
# # The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
# #
# # Examples:
# #
# #   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
# #   Character.create(name: 'Luke', movie: movies.first)

# # art = User.create(name: 'Art    ', email: 'art@art.com', password: '123456')

# # @user.image.attach(io: File.open('/path/to/file'), filename: 'file.pdf')

admin = User.create(name: 'Admin', email: 'admin@admin.admin', password: 'adminadmin', is_admin: true)
wert = User.create(name: 'Wert', email: 'wert@wert.com', password: '123456', password_confirmation: '123456')

def copy_image_fixture(email, obj)
  6.times do |i|
    fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures_path', email, "#{i + 1}.jpg")
    photo = obj.photos.create
    photo.images.attach(io: File.open(fixtures_path), filename: "#{i + 1}.jpg")

    Comment.create(user_id: obj.id, text: Faker::Lorem.words(number: 5), photo_id: photo.id)
  end
end

emails = ['number@11.com', 'number@22.com', 'number@33.com']

emails.each do |email|
  user = User.create(
    email: email,
    name: Faker::Name.name,
    password: '123456',
    password_confirmation: '123456'
  )
  copy_image_fixture(user.email, user)
end
