# frozen_string_literal: true

Rails.application.routes.draw do
  root 'photos#index'

  ActiveAdmin.routes(self)
  devise_for :users

  get 'photos/new' => 'photos#new'
  post 'photos/new' => 'photos#create'

  get 'photos/:photo_id/likes/new', to: 'likes#new', as: 'likes_new'
  get 'likes/:id/destroy', to: 'likes#destroy', as: 'likes_destroy'

  post 'photos/:photo_id/comments/new', to: 'comments#create', as: 'comments_new'

  get 'users/:followed_id/relationships/new', to: 'relationships#new',  as: 'relationships_new'
  get 'relationships/:id/destroy', to: 'relationships#destroy', as: 'relationships_destroy'

  get 'users/:id', to: 'users#show', as: 'users_show'
end
