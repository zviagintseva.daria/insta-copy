# frozen_string_literal: true

class User < ApplicationRecord
  validates :name, presence: true, length: { maximum: 50 }
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :photos, dependent: :destroy
  has_many :active_relationships, class_name: 'Relationship',
                                  foreign_key: 'follower_id',
                                  dependent: :destroy

  has_many :likes
  has_many :comments

  def get_relationship_by_followed(followed_id)
    active_relationships.each do |relationship|
      return relationship if relationship.followed_id == followed_id
    end

    nil
  end
end
