# frozen_string_literal: true

class Photo < ApplicationRecord
  has_many_attached :images

  belongs_to :user
  has_many :likes
  has_many :comments

  def get_like_for_user(user_id)
    likes.each do |like|
      return like if (like.user_id == user_id) && (like.photo_id == id)
    end
    nil
  end
end
