# frozen_string_literal: true

class CommentsController < ApplicationController
  def new
    comment = Comment.new
  end

  def create
    comment = Comment.new(comment_params)
    comment.user = current_user
    comment.photo_id = params[:photo_id]

    if comment.save
      redirect_back(fallback_location: root_path)
    else
      redirect_to comments_new_path
    end
  end

  def comment_params
    params.require(:comment).permit(:text)
  end
  end
