# frozen_string_literal: true

class RelationshipsController < ApplicationController
  def new
    relationship = Relationship.new

    relationship.follower_id = current_user.id
    relationship.followed_id = params[:followed_id]

    relationship.save
    redirect_to users_show_path(params[:followed_id])
  end

  def destroy
    relationship = Relationship.find(params[:id])
    relationship.destroy

    redirect_to users_show_path(relationship.followed_id)
  end
end
