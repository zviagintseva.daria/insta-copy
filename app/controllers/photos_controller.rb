# frozen_string_literal: true

class PhotosController < ApplicationController
  def index
    @photos = Photo.all.order('photos.created_at DESC')
  end

  def new
    @photo = Photo.new
  end

  def create
    photo = Photo.new(photo_params)
    photo.user = current_user
    if photo.save!
      redirect_to root_path
    else
      redirect_to photos_new_path
    end
  end

  private

  def photo_params
    params.require(:photo).permit(images: [])
  end
end
