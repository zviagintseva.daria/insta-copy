# frozen_string_literal: true

class LikesController < ApplicationController
  def new
    like = Like.new

    like.user_id = current_user.id
    like.photo_id = params[:photo_id]

    like.save
    redirect_back(fallback_location: root_path)
  end

  def destroy
    like = Like.find(params[:id])
    like.destroy

    redirect_back(fallback_location: root_path)
  end
  end
